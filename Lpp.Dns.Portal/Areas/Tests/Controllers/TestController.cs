﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lpp.Dns.Portal.Root.Areas.Tests.Controllers
{
    public class TestController : Controller
    {
        protected override void HandleUnknownAction(string actionName)
        {
            try
            {
                this.View(actionName).ExecuteResult(ControllerContext);
            } catch (Exception ex)
            {
                throw new HttpException(404, "Page not found");
            }
        }
    }
}
