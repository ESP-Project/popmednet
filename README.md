# popmednet

**NOTE:** This repository is a fork of the main PopMedNet source code and is maintained by Commonwealth Informatics, Inc. This fork was created to provide better support for ESP. The original repository can ben found at: https://github.com/PopMedNet-Team/popmednet

This will contain the code of the open source code. 

Go into the Folder after cloning the repository \popmednet\Lpp.Dns.Model\SQLScripts\Archive and unzip the file   DNS2_c_optional_MedicalCodes.zip

Instructions for how to set up an instance of PopMedNet is located in the file: PopMedNet Build.docx

The starter databases are too large to upload on Git you can access it through google drive though: https://drive.google.com/file/d/0B8KNecUJscOJd2VnOWw5N2stcmc/view?usp=sharing

Please reach out to us at https://popmednet.atlassian.net/servicedesk/customer/portal/1/group/10 to report any problems.
